'use strict';

const express = require ('express');
const path = require('path');

// Constantes
const PORT = 8081;

// Mi aplicacion
const app = express();

app.use(express.static(__dirname));

app.get('/',function(req,res){
//  res.send("Bienvenido terricola\n");
res.sendFile(path.join(__dirname+'/index.html'));
//Para ir a raiz del proyecto
});

app.get('/detallePost/:id',function(req,res){
//  res.send("Bienvenido terricola\n");
res.sendFile(path.join(__dirname+'/detallePost.html'));
//Para ir al detalle
});

app.get('/admin',function(req,res){
//  res.send("Bienvenido terricola\n");
res.sendFile(path.join(__dirname+'/admin.html'));
//Para ir al detalle
});

app.listen(PORT);
console.log("Express funcionando en el puerto" + PORT);
