var expect = require('chai').expect;
var request = require('request');
var $ = require('chai-jquery');

describe("Pruebas sencillas", function(){

it('Test suma', function () {
  expect(9+4).to.equal(13);

});
});

describe("Pruebas de red", function(){

it('Test internet', function (done) {
  request.get("http://www.forocoches.com",
      function(error,response, body) {
        expect(response.statusCode).to.equal(200);
        done();
  });

});


it('Test puerto', function (done) {
          request.get("http://localhost:8082",
              function(error,response, body) {
                expect(response.statusCode).to.equal(200);
                done();


  });
});


it('Test body', function (done) {
          request.get("http://localhost:8082",
              function(error,response, body) {
                expect(body).contains('<h1>');
                done();


  });
});

});

describe('Test contenido HTML', function(){

it ('Test H1', function(done){
    request.get("http://localhost:8082",
    function(error, response, body){
      expect($('body h1')).to.have.text("Bienvenido a mi blog");
      done();


    });


});

});
