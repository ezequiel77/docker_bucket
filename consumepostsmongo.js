const URL ="https://api.mlab.com/api/1/databases/bootcamp/collections/posts?apiKey=r4_VScEto0eBs4PXFxOXkSSStxK4OU0C";
var response;

function obtenerPosts() {

  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send();
  response = JSON.parse(peticion.responseText);
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  //mostrarPosts();


};

function mostrarPosts(){
  var tabla = document.getElementById("tablaPosts");
for (var i = 0; i < response.length; i++) {
  //alert(response[i].titulo);
  var fila = tabla.insertRow(i+1);
  var celdaId = fila.insertCell(0);
  var celdaTitulo = fila.insertCell(1);
  var celdaTexto = fila.insertCell(2);
  var celdaAutor = fila.insertCell(3);
  var celdaOperaciones = fila.insertCell(4);

  celdaId.innerHTML = response[i]._id.$oid;
  celdaTitulo.innerHTML = response[i].titulo;
  celdaTexto.innerHTML = response[i].texto;
  if (response[i].autor != undefined)
  {
  celdaAutor.innerHTML = response[i].autor.nombre + "" + response[i].autor.apellido;
  }
  else {
    celdaAutor.innerHTML = "Anonimo";
  }
  celdaOperaciones.innerHTML = '<button onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>'
}

};

function anadirPost(){

  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Nuevo post desde Atom", "texto":"Nuevo texto desde Atom","autor":{"nombre":"Fernando","apellido":"Sanchez"}}');

};

function actualizarPost(id) {

  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=r4_VScEto0eBs4PXFxOXkSSStxK4OU0C";
  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Titulo cambiado"}');

};

function borrarPost (id) {

  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/bootcamp/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=r4_VScEto0eBs4PXFxOXkSSStxK4OU0C";
  peticion.open("DELETE", URLItem, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  //peticion.send('{"titulo":"Titulo cambiado"}');

};

function seleccionarPost (numero) {
    sessionStorage["seleccionado"]=numero;
};

function buscarDetallesPost(numero) {
  var posts = JSON.parse(sessionStorage["posts"]);
  for (var i = 0; i< posts.length; i++) {
    if (posts[i]._id.$oid == numero)
    {
      //Mostrar detalles
      document.getElementById("h1").innerHTML = numero;
      document.getElementById("h2").innerHTML = posts[i].titulo;
      document.getElementById("h3").innerHTML = posts[i].texto;      

      break;
    }
  }
}
